﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    class Policy
    {
        /// <summary>
        /// The policy of the letter, return false if the input is not correct
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool TextPolicy(string name)
        {
            char[] tmp = name.ToLower().ToCharArray();
            foreach (char c in tmp)
            {
                if (!((int)c > 96 && (int)c < 123))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
