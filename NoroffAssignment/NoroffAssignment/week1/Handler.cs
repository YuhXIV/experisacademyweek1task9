﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoroffAssignment.week1
{
    class InputHandler
    {
        Policy policy = new Policy();
        AnimalKingdom animalkingdom;
        string input;
        public bool exit = false;
        public InputHandler(int choice, AnimalKingdom animalKingdom)
        {
            this.animalkingdom = animalKingdom;
            if (choice == 4) exit = true;
            else if (choice == 3) Roar();
            else if (choice == 2) Search();
            else if (choice == 1) Breed();
        }

        public void Roar()
        {
            for (int i = 0; i < animalkingdom.getAnimalList().Count; i++)
            {
                Console.Write($"{animalkingdom.getAnimalList().ElementAt(i).Name} [{i+1}]  ");
            }
            Console.Write($"All[{animalkingdom.getAnimalList().Count+2}]");
            string input = Console.ReadLine();
            int choice;
            if (!int.TryParse(input, out choice) || choice > animalkingdom.getAnimalList().Count+1) {
                Console.WriteLine("Not a valid choice");
                return;
            }
            if (choice == animalkingdom.getAnimalList().Count + 1)
            {
                for (int i = 0; i < animalkingdom.getAnimalList().Count; i++)
                {
                    Console.WriteLine($"{animalkingdom.getAnimalList().ElementAt(i).Roar}");
                }
            }
            else
            {
                Console.WriteLine($"{animalkingdom.getAnimalList().ElementAt(choice-1).Roar}");
            }

        }

        /// <summary>
        /// prompt the search function in the console
        /// </summary>
        public void Search()
        {
            Console.WriteLine("Enter the animal name: ");
            input = Console.ReadLine();
            if (!policy.TextPolicy(input))
            {
                Console.WriteLine("Invalid input");
                return;
            }
            Console.WriteLine("Matching animal: ");
            for (int i = 0; i < animalkingdom.FindAllByName(input).Count; i++)
            {
                Console.WriteLine(animalkingdom.FindAllByName(input).ElementAt(i).Name);
            }
        }

        /// <summary>
        /// prompt all necessary variable that is needed to create an animal
        /// </summary>
        public void Breed()
        {
            Console.WriteLine("Enter the animal name: ");
            string name = Console.ReadLine();
            if (!policy.TextPolicy(name)) return;
            Console.WriteLine($"Enter the {name} age: ");
            int age;
            string tmp = Console.ReadLine();
            int.TryParse(tmp, out age);
            if (!int.TryParse(tmp, out age)) return;
            Console.WriteLine($"Enter the {name} species: ");
            string species = Console.ReadLine();
            if (!policy.TextPolicy(species)) return;

            ConsoleKey response;
            do
            {
                //prompt questien of displaying input.
                Console.Write("Do you want to add weight and height? [y/n]: ");
                response = Console.ReadKey(false).Key;
                if (response != ConsoleKey.Enter) //if enter is pressed, nothing happen
                    Console.WriteLine();

            } while (response != ConsoleKey.Y && response != ConsoleKey.N);



            bool animalAdded = false;
            //only add weight and height if the user wish to
            if (response == ConsoleKey.N)
            {
                animalAdded = animalkingdom.BreedAnimal(name, species, age);
            }
            else
            {
                Console.WriteLine($"Enter the {name} weight: ");
                double weight;
                string tmp2 = Console.ReadLine();
                if (!double.TryParse(tmp2, out weight)) return;

                Console.WriteLine($"Enter the {name} height: ");
                double height;
                tmp2 = Console.ReadLine();
                if(!double.TryParse(tmp2, out height)) return; ;

                animalAdded = animalkingdom.BreedAnimal(name, species, age, weight, height);
            }
            
            if(animalAdded)
            {
                Console.WriteLine($"{name} added");
            } else
            {
                Console.WriteLine($"{name} not added");
            }
        }
    }
}
