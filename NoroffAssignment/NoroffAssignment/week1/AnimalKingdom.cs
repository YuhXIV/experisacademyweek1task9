﻿using NoroffAssignment.week1.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    class AnimalKingdom
    {
        private List<Animal> animalList;
        public AnimalKingdom()
        {
            Animal tiger = new Animal("tiger", "Thomas", 23, 50, 50);
            Animal cat = new Animal("cat", "Nils", 2);
            Animal dog = new Animal("dog", "Theo", 7);
            this.animalList = new List<Animal>();
            animalList.Add(tiger);
            animalList.Add(cat);
            animalList.Add(dog);
            tiger.Roar = "Roooooooooooaaar!!!! *imma gonna eat ya!*";
            cat.Roar = "MAAAAAOOOOOWWWWW *whimps....*";
            dog.Roar = "bark..... *to lazy to do anything*";


        }
        /// <summary>
        /// main function to create a new object of animals
        /// </summary>
        /// <param name="name"></param>
        /// <param name="species"></param>
        /// <param name="age"></param>
        /// <param name="weight"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public bool BreedAnimal(string name, string species, int age, double weight = -1, double height = -1)
        {
            Animal animal;
            if (weight == -1)
            {
                animal = new Animal(species, name, age);
            }else
            {
                animal = new Animal(species, name, age, weight, height);
            }
            animalList.Add(animal);
            if(CheckAnimal(animal))
            {
                return true;
            } else
            {
                return false;
            }
        }
        /// <summary>
        /// Check if the animal exist
        /// </summary>
        /// <param name="animal"></param>
        /// <returns></returns>
        public bool CheckAnimal(Animal animal)
        {
            if (animalList.Contains(animal))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Find all animal that matches the input
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<Animal> FindAllByName(string name)
        {
            List<Animal> matchingAnimal = new List<Animal>();

            foreach(Animal a in animalList)
            {
                if (a.Matching(name)) matchingAnimal.Add(a);
            }

            return matchingAnimal;
        }
        public List<Animal> getAnimalList()
        {
            return animalList;
        }
    }
}