﻿using System;
using NoroffAssignment.week1;

namespace NoroffAssignment
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the animal kingdom!");
            AnimalKingdom animalKingdom = new AnimalKingdom();
            InputHandler handler;
            string input;
            int choice;
            do
            {
                Console.WriteLine("Make your choice!");
                Console.WriteLine("Registrate animal[1] Find animal[2] Behavior[3] Exit[4]");
                input = Console.ReadLine();

                if (!int.TryParse(input, out choice) || choice > 4)
                {
                    Console.WriteLine("Not a valid choice\n");
                    continue;
                }
                handler = new InputHandler(choice, animalKingdom);
                if (handler.exit) break;

            } while (true);
            Console.WriteLine("We welcome you back to our animal kingdom!\n\n");
       
        }
    }
}
